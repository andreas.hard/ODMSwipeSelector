//
//  ODMSwipeSelector.h
//  ODMSwipeSelector
//
//  Created by Oscar De Moya on 10/24/14.
//  Copyright (c) 2014 Oscart. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ODMSwipeSelectorDelegate <NSObject>
@required
- (void)swipeGestureDidEnd;
@end

IB_DESIGNABLE
@interface ODMSwipeSelector : UIView

@property (nonatomic) IBInspectable UIColor *minSwipingColor;
@property (nonatomic) IBInspectable UIColor *maxSwipingColor;
@property (nonatomic) IBInspectable UIColor *defaultLabelColor;
@property (nonatomic) IBInspectable UIColor *swipingLabelColor;
@property (nonatomic) IBInspectable UIColor *defaultBackgroundColor;
@property (nonatomic) IBInspectable NSString *title;
@property (nonatomic, weak) id<ODMSwipeSelectorDelegate> delegate;

@end
